const ISO: i32 = 200;

export function drawDiagram(data: Uint8ClampedArray, width: i32, height: i32, mouseX: i32, mouseY: i32, power: f32): void {
  if (width * height * 4 > data.length) {
    return;
  }

  for (let j: i32 = 0; j < width; j++) {
    let l: f64 = j / (width as f64) * 2 + 2;
    let x = j / (width as f64);
    for (let i: i32 = 0; i < 100; i++) {
      x = x * l * (1 - x)
    }

    for (let i: i32 = 0; i < ISO; i++) {
      x = x * l * (1 - x)

      const dY: f64 = x - (1.0 - ((mouseY as f64) / (height as f64)));
      const dX: f64 = l - ((mouseX as f64) / (width as f64) * 2.0 + 2.0);

      const d2 = dX * dX + dY * dY

      if (d2 !== 0) {
        const r = power * 0.0000001 / (d2);
        const theta: f64 = Math.atan2(dY, dX);
        x += r * Math.sin(theta);
        // l += r * Math.cos(theta) / 100;
      }

      let xPos: i32 = Math.max(0, Math.min(width, Math.round((l - 2) / 2 * width))) as i32;
      let yPos: i32 = Math.max(0, Math.min(height, Math.round(height - x * height))) as i32;

      let idx: i32 = (xPos + yPos * width) * 4;

      if (idx + 3 < data.length) {
        data[idx + 3] += Math.min(255, (6000.0 / (ISO as f64)) as i32) as u8;
      }
    }
  }
}

export const PixelArray_ID = idof<Uint8ClampedArray>()
